const baseFolder = "assets/";

export const CIRCLE_ICON_PATH = baseFolder + "circle.svg";
export const SQUARE_ICON_PATH = baseFolder + "square.svg";
export const TRIANGLE_ICON_PATH = baseFolder + "triangle.svg";
