import { CountriesCoordinates } from "../constants/countries-coordinates";
import { calculateRandomCoordinates } from "../utils/calculate-random-coordinates";
import { TriangleFigure } from "../classes/triangle-figure";

export const handleSendTriangle =
  (leafletService, { featureGroup }) =>
  () => {
    const destination = calculateRandomCoordinates();

    const figure = new TriangleFigure(leafletService, {
      trajectoryTargetLatLngs: [CountriesCoordinates.ESTONIA, destination],
      featureGroup,
    });

    figure.send();
  };
