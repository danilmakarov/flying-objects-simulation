import { SquareFigure } from "../classes/square-figure";

export const squarePathCoords = [
  [59.41376714444269, 24.83925893982419],
  [-60.409962, -158.203125],
  [-50.409962, -140.203125],
  [59.41376714444269, 24.83925893982419],
];

export const handleSendSquare = (leafletService, { featureGroup }) => {
  return () => {
    const figure = new SquareFigure(leafletService, {
      trajectoryTargetLatLngs: squarePathCoords,
      featureGroup,
    });
    figure.send();
  };
};
