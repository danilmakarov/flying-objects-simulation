export const handleToggleFeatureGroup = (map, featureGroup) => () => {
  if (map.hasLayer(featureGroup)) {
    return map.removeLayer(featureGroup);
  }
  return featureGroup.addTo(map);
};
