import { getRandomNumberBetween } from "../utils/get-random-number-between";
import { Coordinates } from "../stored-data/coordinates";
import { calculatePointOnGivenVectorAndDistance } from "../utils/calculate-point-on-given-vector-and-distance";
import { CountriesCoordinates } from "../constants/countries-coordinates";
import { CircleFigure } from "../classes/circle-figure";

export const circleTrajectoryRange = [9000, 29000];

export const handleSendCircle =
  (leafletService, { featureGroup }) =>
  () => {
    const circleTrajectoryRadius = getRandomNumberBetween(
      ...circleTrajectoryRange,
    );

    const circleTrajectoryCoordinates =
      Coordinates.ESTONIA_TALLINN_1000M_CIRCLE.map((latlng) =>
        calculatePointOnGivenVectorAndDistance(
          CountriesCoordinates.ESTONIA,
          latlng,
          circleTrajectoryRadius,
        ),
      );

    const figure = new CircleFigure(leafletService, {
      trajectoryTargetLatLngs: circleTrajectoryCoordinates,
      featureGroup,
    });

    figure.send();
  };
