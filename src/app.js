import L from "leaflet";
import { MapConfig } from "./configs/map-config";
import { handleSendSquare } from "./handlers/handle-send-square";
import { LeafletService } from "./services/leaflet-service";
import { handleSendCircle } from "./handlers/handle-send-circle";
import { handleToggleFeatureGroup } from "./handlers/handle-toggle-feature-group";
import { handleSendTriangle } from "./handlers/handle-send-triangle";
import { checkFiguresAmountProxy } from "./utils/check-figures-amount-proxy";
import { MAX_FIGURES_AMOUNT } from "./configs/default-leaflet-figures-config";

const sendSquareBtn = document.getElementById("button-square");
const sendCircleBtn = document.getElementById("button-circle");
const sendTriangleBtn = document.getElementById("button-triangle");

const toggleTriangleBtn = document.getElementById("triangle-toggle");
const toggleSquareBtn = document.getElementById("square-toggle");
const toggleCircleBtn = document.getElementById("circle-toggle");

const leafletService = new LeafletService(L);

const map = leafletService
  .createMap(MapConfig.MAP_ELEMENT_ID, MapConfig.MAP_OPTIONS)
  .setView(MapConfig.MAP_INITIAL_VIEW, MapConfig.MAP_INITIAL_ZOOM);

const triangleFeatureGroup = leafletService.createAndAttachFeatureGroup();
const squareFeatureGroup = leafletService.createAndAttachFeatureGroup();
const circleFeatureGroup = leafletService.createAndAttachFeatureGroup();

const featureGroups = [
  triangleFeatureGroup,
  squareFeatureGroup,
  circleFeatureGroup,
];

const amountChecker = checkFiguresAmountProxy(
  featureGroups,
  MAX_FIGURES_AMOUNT,
);

// Send object handlers
sendSquareBtn.addEventListener(
  "click",
  amountChecker(
    handleSendSquare(leafletService, { featureGroup: squareFeatureGroup }),
  ),
);
sendCircleBtn.addEventListener(
  "click",
  amountChecker(
    handleSendCircle(leafletService, { featureGroup: circleFeatureGroup }),
  ),
);
sendTriangleBtn.addEventListener(
  "click",
  amountChecker(
    handleSendTriangle(leafletService, { featureGroup: triangleFeatureGroup }),
  ),
);

// Toggle objects display handlers
toggleTriangleBtn.addEventListener(
  "click",
  handleToggleFeatureGroup(map, triangleFeatureGroup),
);
toggleSquareBtn.addEventListener(
  "click",
  handleToggleFeatureGroup(map, squareFeatureGroup),
);
toggleCircleBtn.addEventListener(
  "click",
  handleToggleFeatureGroup(map, circleFeatureGroup),
);
