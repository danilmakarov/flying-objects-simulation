export const DefaultTrajectoryPolylineConfig = {
  steps: 6,
};

// Increase intervalTime and maxLocations on higher speeds to show the trail properly
export const DefaultTalePolylineConfig = {
  polylineOptions: { color: "red", weight: 6, smoothFactor: 0.1 },
};

export const DefaultMarkerOptions = {
  riseOnHover: true,
};

export const DefaultPopupOptions = {
  className: "marker-popup",
  keepInView: false,
};

export const MAX_FIGURES_AMOUNT = 5000;
