import L from "leaflet";
import { MapTileLayerConfig } from "./map-tile-layer-config";
import { ControlPositions } from "../constants/control-positions";

const centerLat = 50.0;
const centerLng = 10.0;

const maxBounds = L.latLngBounds(
  L.latLng(-90, -Infinity),
  L.latLng(90, Infinity),
);

export const MapConfig = {
  MAP_ELEMENT_ID: "map",
  MAP_INITIAL_VIEW: [centerLat, centerLng],
  MAP_INITIAL_ZOOM: 3,
  MAP_OPTIONS: {
    worldCopyJump: true,
    zoomControl: false,
    center: [centerLat, centerLng],
    layers: [
      L.tileLayer(
        MapTileLayerConfig.TILE_LAYER_URL,
        MapTileLayerConfig.TILE_LAYER_OPTIONS,
      ),
    ],
    maxBoundsViscosity: 1,
    maxBounds,

    // leaflet-rotate options
    rotate: true,
    rotateControl: {
      position: ControlPositions.TOP_RIGHT,
      closeOnZeroBearing: false,
    },
  },
};
