export const MapTileLayerConfig = {
  TILE_LAYER_URL: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
  TILE_LAYER_OPTIONS: { maxZoom: 11, minZoom: 2 },
};
