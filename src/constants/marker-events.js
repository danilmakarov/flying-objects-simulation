export const MarkerEvents = {
  POPUP_OPEN: "popupopen",
  POPUP_CLOSE: "popupclose",
  END: "end",
  REMOVE: "remove",
};
