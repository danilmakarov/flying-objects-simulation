export const TimeInMilliseconds = {
  HOUR: 60 * 60 * 1000,
  MINUTE: 60 * 1000,
  HALF_MINUTE: 30 * 1000,
  SECOND: 1000,
  "2SECOND": 2000,
  "3SECOND": 3000,
  "6SECOND": 6000,
  HALF_SECOND: 500,
};
