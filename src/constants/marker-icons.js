import L from "leaflet";
import {
  CIRCLE_ICON_PATH,
  SQUARE_ICON_PATH,
  TRIANGLE_ICON_PATH,
} from "../../assets";

export const MarkerIcons = {
  squareIcon: L.icon({
    iconUrl: SQUARE_ICON_PATH,
    iconSize: [25, 25],
    iconAnchor: [10, 10],
    popupAnchor: [2, -10],
  }),
  triangleIcon: L.icon({
    iconUrl: TRIANGLE_ICON_PATH,
    iconSize: [30, 30],
    iconAnchor: [15, 15],
    popupAnchor: [0, -10],
  }),
  circleIcon: L.icon({
    iconUrl: CIRCLE_ICON_PATH,
    iconSize: [30, 30],
    iconAnchor: [10, 10],
    popupAnchor: [5, -10],
  }),
};
