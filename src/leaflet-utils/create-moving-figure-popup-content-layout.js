export const createMovingFigurePopupContentLayout = (layoutData) => {
  return Object.entries(layoutData)
    .map(
      (content) =>
        ` <span><strong>${content[0]}: </strong>${content[1]}</span>`,
    )
    .join("<br/>");
};
