import { MovingFigure } from "./moving-figure";
import { getRandomNumberBetween } from "../utils/get-random-number-between";
import {
  DefaultMarkerOptions,
  DefaultTalePolylineConfig,
} from "../configs/default-leaflet-figures-config";
import { MarkerIcons } from "../constants/marker-icons";
import { TimeInMilliseconds } from "../constants/time-in-milliseconds";
import { MarkerEvents } from "../constants/marker-events";
import { getTimeDifference } from "../utils/get-time-difference-in-minutes";

const triangleSpeedRange = [1700, 2200];

export class TriangleFigure extends MovingFigure {
  _lifetime = TimeInMilliseconds.HOUR;
  _expiringTime = Date.now() + this._lifetime;

  constructor(leafletService, figureOptions) {
    super(leafletService, figureOptions);

    this._speed = getRandomNumberBetween(...triangleSpeedRange);

    this._tailPolylineConfig = {
      ...DefaultTalePolylineConfig,
      intervalTime: TimeInMilliseconds["2SECOND"],
      maxLocations: 30,
    };

    this._markerOptions = {
      ...DefaultMarkerOptions,
      icon: MarkerIcons.triangleIcon,
    };
  }

  _setLifetime() {
    setTimeout(() => this._marker.fire(MarkerEvents.END), this._lifetime);
  }

  _getDynamicPopupContent() {
    const originContent = super._getDynamicPopupContent();
    const triangleContent = {
      ["Expires in"]: getTimeDifference(this._expiringTime, Date.now()),
    };
    return { ...originContent, ...triangleContent };
  }

  send() {
    this._setLifetime();
    return super.send();
  }
}
