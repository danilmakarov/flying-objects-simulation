import { MovingFigure } from "./moving-figure";
import { getRandomNumberBetween } from "../utils/get-random-number-between";
import {
  DefaultTrajectoryPolylineConfig,
  DefaultMarkerOptions,
} from "../configs/default-leaflet-figures-config";
import { MarkerIcons } from "../constants/marker-icons";

const speedRange = [110, 300];

export class CircleFigure extends MovingFigure {
  constructor(leafletService, figureOptions) {
    super(leafletService, figureOptions);

    this._speed = getRandomNumberBetween(...speedRange);

    this._trajectoryPolylineConfig = {
      ...DefaultTrajectoryPolylineConfig,
      steps: 1,
    };

    this._markerOptions = {
      ...DefaultMarkerOptions,
      icon: MarkerIcons.circleIcon,
    };
  }
}
