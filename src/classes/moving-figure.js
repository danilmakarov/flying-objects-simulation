import { TimeInMilliseconds } from "../constants/time-in-milliseconds";
import { MarkerEvents } from "../constants/marker-events";
import { createMovingFigurePopupContentLayout } from "../leaflet-utils/create-moving-figure-popup-content-layout";
import {
  DefaultTrajectoryPolylineConfig,
  DefaultMarkerOptions,
  DefaultPopupOptions,
  DefaultTalePolylineConfig,
} from "../configs/default-leaflet-figures-config";
import { formatNumber } from "../utils/format-number";
import { Metrics } from "../constants/metrics";

export class MovingFigure {
  _marker;
  _markerOptions = DefaultMarkerOptions;

  _popup;
  _popupOptions = DefaultPopupOptions;
  _popupUpdateInterval = TimeInMilliseconds.HALF_SECOND;
  _popupCoordsDisplayAccuracy = 10;

  _leafletService;

  _speed;
  _distance;

  _trajectoryPolyline;
  _trajectoryTargetLatLngs;
  _trajectoryGreatCircleLatLngs;
  _trajectoryPolylineConfig = DefaultTrajectoryPolylineConfig;

  _tailPolyline;
  _tailPolylineConfig = DefaultTalePolylineConfig;
  _tailPolylineUpdateInterval = TimeInMilliseconds.HALF_SECOND;
  _tailLengthMetersAmount;
  _tailLengthMsAmount = TimeInMilliseconds.MINUTE;

  constructor(leafletService, { featureGroup, trajectoryTargetLatLngs }) {
    this._leafletService = leafletService;
    this._featureGroup = featureGroup;
    this._trajectoryTargetLatLngs = trajectoryTargetLatLngs;
  }

  _calculateFlyTime() {
    const speedKmPerMs = this._speed / TimeInMilliseconds.HOUR;
    return this._distance / speedKmPerMs;
  }

  _calculateTailLength() {
    const speedKmH = this._speed;
    const timeMs = this._tailLengthMsAmount;

    const speedMS = (speedKmH * 1000) / 3600;
    const timeS = timeMs / 1000;

    return speedMS * timeS;
  }

  _getStaticPopupContent() {
    return {
      Speed: `${this._speed} ${Metrics.KM_PER_HOUR}`,
      Distance: `${formatNumber(this._distance)} ${Metrics.KM}`,
    };
  }

  _getDynamicPopupContent() {
    const { lat, lng } = this._marker.getLatLng();
    const [cutLat, cutLong] = [
      lat.toFixed(this._popupCoordsDisplayAccuracy),
      lng.toFixed(this._popupCoordsDisplayAccuracy),
    ];

    const position = `<br/> lat: ${cutLat}, <br/> lng: ${cutLong}`;

    return {
      Position: position,
    };
  }

  _configurePopupContentUpdate() {
    let interval;

    const popupContent = this._getStaticPopupContent();

    const updateContent = () => {
      const content = createMovingFigurePopupContentLayout({
        ...popupContent,
        ...this._getDynamicPopupContent(),
      });
      this._popup.setContent(content);
    };

    this._marker.on(MarkerEvents.POPUP_OPEN, () => {
      interval = setInterval(updateContent, this._popupUpdateInterval);
      updateContent();
    });

    this._marker.on(MarkerEvents.POPUP_CLOSE, () => {
      clearInterval(interval);
    });
  }

  _calculateMarkerPrevLatLng(targetLatLng) {
    const trajectoryLatLngs = this._trajectoryGreatCircleLatLngs;
    const distanceChecker = this._leafletService.getDistance.bind(
      this._leafletService,
    );

    for (let idx = 0; idx < trajectoryLatLngs.length - 1; idx++) {
      const tempLatLng1 = trajectoryLatLngs[idx];
      const tempLatLng2 = trajectoryLatLngs[idx + 1];

      const targetToTemp1Dist = distanceChecker(targetLatLng, tempLatLng1);
      const targetToTemp2Dist = distanceChecker(targetLatLng, tempLatLng2);
      const distBetweenSegments = Math.trunc(
        distanceChecker(tempLatLng1, tempLatLng2),
      );
      const distBetweenTargetAndTempLatLngs = Math.trunc(
        targetToTemp1Dist + targetToTemp2Dist,
      );
      const distDiff = Math.abs(
        distBetweenSegments - distBetweenTargetAndTempLatLngs,
      );

      if (distDiff > 100) {
        continue;
      }

      return {
        markerPrevLatLng: idx,
        distBetweenSegments: distBetweenSegments,
      };
    }
  }

  _calculateLatestLatLngs(lastLatLngIdx, distBetweenSegments) {
    const currentMarkerLatLng = this._marker.getLatLng();
    const trajectoryLatLngs = this._trajectoryGreatCircleLatLngs;
    const tailDistance = this._tailLengthMetersAmount;

    const resultLatLngs = [
      currentMarkerLatLng,
      trajectoryLatLngs[lastLatLngIdx],
    ];

    const numberOfSegmentsInTail = Math.ceil(
      tailDistance / distBetweenSegments,
    );

    for (let idx = 1; idx <= numberOfSegmentsInTail; idx++) {
      const segmentIdx = lastLatLngIdx - idx;
      const segmentLatLng = trajectoryLatLngs[segmentIdx];
      if (!segmentLatLng) {
        break;
      }
      resultLatLngs.push(segmentLatLng);
    }

    return resultLatLngs;
  }

  _cutLatLngsByDistance(latLongs, targetDistance) {
    const { map } = this._leafletService;
    let index = 0;
    let accumulatedDistance = 0;
    let step = 5;
    let chosenIndex = 0;

    while (index < latLongs.length - 1) {
      if (index + step >= latLongs.length) {
        step--;
        continue;
      }

      const segmentDistance = map.distance(
        latLongs[index],
        latLongs[index + step],
      );

      if (accumulatedDistance + segmentDistance > targetDistance) {
        if (step === 1) {
          chosenIndex = index;
          break;
        }
        step--;
        continue;
      }

      accumulatedDistance += segmentDistance;
      index += step;
      chosenIndex = index;
    }

    return latLongs.slice(0, chosenIndex + 1);
  }

  _calculateTailLatLngs() {
    const currentMarkerLatLng = this._marker.getLatLng();
    const tailDistance = this._tailLengthMetersAmount;

    const { markerPrevLatLng, distBetweenSegments } =
      this._calculateMarkerPrevLatLng(currentMarkerLatLng);
    const markerLatestLatLngs = this._calculateLatestLatLngs(
      markerPrevLatLng,
      distBetweenSegments,
    );

    const latestLatLngsPolyline =
      this._leafletService.createPolylineGreatCircleRoute(markerLatestLatLngs, {
        steps: 5,
      });
    const latestLatLngs = latestLatLngsPolyline.getLatLngs().flat();

    const latestLatLngsDistance = latestLatLngsPolyline.getDistanceKm() * 1000;

    if (latestLatLngsDistance <= tailDistance) {
      return latestLatLngs;
    }

    return this._cutLatLngsByDistance(latestLatLngs, tailDistance);
  }

  _configureTailPolyline() {
    let updateInterval;

    this._tailPolyline = this._leafletService.createPolyline(
      [],
      this._tailPolylineConfig.polylineOptions,
    );

    this._tailLengthMetersAmount = this._calculateTailLength();

    const updateTailPolyline = () => {
      const tailLatLongs = this._calculateTailLatLngs();
      this._tailPolyline.setLatLngs(tailLatLongs);
    };

    this._marker.on(MarkerEvents.POPUP_OPEN, () => {
      updateTailPolyline();

      updateInterval = setInterval(
        updateTailPolyline,
        this._tailPolylineUpdateInterval,
      );
    });

    this._marker.on(MarkerEvents.POPUP_CLOSE, () => {
      clearInterval(updateInterval);
    });
  }

  _configureTrajectoryPolyline() {
    this._trajectoryPolyline =
      this._leafletService.createPolylineGreatCircleRoute(
        this._trajectoryTargetLatLngs,
        this._trajectoryPolylineConfig,
      );

    this._trajectoryGreatCircleLatLngs = this._trajectoryPolyline
      .getLatLngs()
      .flat();

    this._distance = this._trajectoryPolyline.getDistanceKm();
  }

  _configurePolylinesDisplay() {
    this._marker.on(MarkerEvents.POPUP_OPEN, () => {
      this._trajectoryPolyline.addTo(this._leafletService.map);
      this._tailPolyline.addTo(this._leafletService.map);
    });

    this._marker.on(MarkerEvents.POPUP_CLOSE, () => {
      this._trajectoryPolyline.remove();
      this._tailPolyline.remove();
    });
  }

  _configureMarker() {
    const { marker, popup } = this._leafletService.createMarkerMoving(
      this._trajectoryGreatCircleLatLngs,
      this._calculateFlyTime(),
      this._markerOptions,
      this._popupOptions,
    );
    this._marker = marker;
    this._popup = popup;
  }

  _configureFeatureGroupConnection() {
    this._featureGroup.addLayer(this._marker);

    this._marker.on(MarkerEvents.END, () =>
      this._featureGroup.removeLayer(this._marker),
    );
  }

  send() {
    this._configureTrajectoryPolyline();
    this._configureMarker();
    this._configureTailPolyline();
    this._configurePolylinesDisplay();
    this._configurePopupContentUpdate();
    this._configureFeatureGroupConnection();

    this._marker.start();
  }
}
