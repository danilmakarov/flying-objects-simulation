import { MovingFigure } from "./moving-figure";
import { getRandomNumberBetween } from "../utils/get-random-number-between";
import {
  DefaultTrajectoryPolylineConfig,
  DefaultMarkerOptions,
} from "../configs/default-leaflet-figures-config";
import { MarkerIcons } from "../constants/marker-icons";

const speedRange = [50, 80];

export class SquareFigure extends MovingFigure {
  constructor(leafletService, figureOptions) {
    super(leafletService, figureOptions);

    this._speed = getRandomNumberBetween(...speedRange);

    this._trajectoryPolylineConfig = {
      ...DefaultTrajectoryPolylineConfig,
      steps: 6,
    };

    this._markerOptions = {
      ...DefaultMarkerOptions,
      icon: MarkerIcons.squareIcon,
      loop: true,
    };
  }
}
