import { ErrorMessages } from "../constants/error-messages";

export const checkFiguresAmountProxy =
  (featureGroups, maxAmount) => (callback) => (event) => {
    const activeFiguresAmount = featureGroups.reduce(
      (acc, group) => acc + group.getLayers().length,
      0,
    );

    if (activeFiguresAmount > maxAmount - 1) {
      return alert(ErrorMessages.OBJECTS_LIMIT + ` (${maxAmount})`);
    }

    return callback(event);
  };
