const [latDefaultBounds, lngDefaultBounds] = [
  [-75, 75],
  [-170, 170],
];

export const calculateRandomCoordinates = (
  latBounds = latDefaultBounds,
  lngBounds = lngDefaultBounds,
) => {
  const lat = Math.random() * (latBounds[1] - latBounds[0] + 1) + latBounds[0];
  const lng = Math.random() * (lngBounds[1] - lngBounds[0] + 1) + lngBounds[0];

  return [lat, lng];
};
