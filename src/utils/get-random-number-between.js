export const getRandomNumberBetween = (min, max) => {
  return Math.trunc(Math.random() * (max - min) + min);
};
