const toRadians = (degrees) => (degrees * Math.PI) / 180;
const toDegrees = (radians) => (radians * 180) / Math.PI;

/**
 * Function calculates the point on a given distance
 * from point2, on a point1-point2 vector.
 * @param {[number,number]} point1
 * @param {[number,number]} point2
 * @param {number} distance
 * @returns {[number,number]}
 */
export const calculatePointOnGivenVectorAndDistance = (
  point1,
  point2,
  distance,
) => {
  const [lat1, lng1] = point1;
  const [lat2, lng2] = point2;

  // Convert degrees to radians
  const phi1 = toRadians(lat1);
  const lambda1 = toRadians(lng1);
  const phi2 = toRadians(lat2);
  const lambda2 = toRadians(lng2);

  // Calculate the bearing angle
  const theta = Math.atan2(
    Math.sin(lambda2 - lambda1) * Math.cos(phi2),
    Math.cos(phi1) * Math.sin(phi2) -
      Math.sin(phi1) * Math.cos(phi2) * Math.cos(lambda2 - lambda1),
  );

  // Earth's radius in meters
  const R = 6371000;
  const delta = distance / R;

  // Calculate the new latitude from the second point
  const phi3 = Math.asin(
    Math.sin(phi2) * Math.cos(delta) +
      Math.cos(phi2) * Math.sin(delta) * Math.cos(theta),
  );

  // Calculate the new longitude from the second point
  const lambda3 =
    lambda2 +
    Math.atan2(
      Math.sin(theta) * Math.sin(delta) * Math.cos(phi2),
      Math.cos(delta) - Math.sin(phi2) * Math.sin(phi3),
    );

  // Convert radians to degrees
  const lat3 = toDegrees(phi3);
  const lng3 = toDegrees(lambda3);

  return [lat3, lng3];
};
