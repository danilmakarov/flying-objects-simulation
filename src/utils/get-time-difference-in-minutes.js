export const getTimeDifference = (time1Ms, time2Ms) => {
  const diffMs = Math.abs(time1Ms - time2Ms);
  const minutes = Math.floor(diffMs / 60000);
  const seconds = Math.floor((diffMs % 60000) / 1000);
  return `${minutes}m ${seconds}s`;
};
