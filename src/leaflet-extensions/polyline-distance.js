import L from "leaflet";

L.Polyline = L.Geodesic.include({
  getDistanceKm() {
    return (this.statistics.totalDistance / 1000).toFixed(0);
  },
});
