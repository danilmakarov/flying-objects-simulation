import "leaflet-rotate";
import "leaflet.geodesic";
import "../leaflet-extensions/moving-marker";
import "../leaflet-extensions/polyline-distance";

export class LeafletService {
  _leaflet;
  _map;

  get map() {
    if (!this._map) {
      throw new Error("Map was not initialized");
    }
    return this._map;
  }

  set map(map) {
    if (!this._map) {
      this._map = map;
    }
  }

  constructor(leaflet) {
    this._leaflet = leaflet;
  }

  createMap(elementId, mapOptions) {
    this.map = this._leaflet.map(elementId, mapOptions);

    this.map.addControl(
      new this._leaflet.Control.Zoom({ position: "bottomright" }),
    );

    return this.map;
  }

  createPolyline(latlngs, options) {
    return new this._leaflet.Polyline(latlngs, options);
  }

  createPolylineGreatCircleRoute(latlngs, options) {
    return new this._leaflet.Geodesic(latlngs, options);
  }

  createMarkerMoving(latLengs, moveTime, markerOptions, popupOptions) {
    const popup = this._leaflet.popup(popupOptions);
    const marker = this._leaflet.Marker.movingMarker(
      latLengs,
      moveTime,
      markerOptions,
    ).bindPopup(popup);
    return { marker, popup };
  }

  createAndAttachFeatureGroup(layers, options) {
    return this._leaflet.featureGroup(layers, options).addTo(this.map);
  }

  getDistance(latLng1, latLng2) {
    return this.map.distance(latLng1, latLng2);
  }
}
