export class EventEmitter extends EventTarget {
  emit(eventName) {
    return this.dispatchEvent(new Event(eventName));
  }
  on(eventName, callback) {
    return this.addEventListener(eventName, callback);
  }
  remove(eventName, callback) {
    return this.removeEventListener(eventName, callback);
  }
}
