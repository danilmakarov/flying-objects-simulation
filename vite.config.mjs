import { defineConfig, normalizePath } from "vite";
import { viteStaticCopy } from "vite-plugin-static-copy";
import * as path from "path";

/**
 * @type {import('vite').UserConfig}
 * */
export default defineConfig({
  base: "./",
  build: {
    outDir: './public'
  },
  plugins: [
    viteStaticCopy({
      targets: [
        {
          src: normalizePath(path.resolve(__dirname, "./assets")) + "/[!.]*",
          dest: "./assets",
        },
      ],
    }),
  ],
});
